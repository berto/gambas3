#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gb.db.form 3.18.90\n"
"PO-Revision-Date: 2023-03-02 01:53 UTC\n"
"Last-Translator: benoit <benoit@benoit-TOWER>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: FBlobEditor.class:29
msgid "&1 byte(s)"
msgstr ""

#: FBlobEditor.form:15
msgid "Blob contents"
msgstr "Blob inhoud"

#: FBlobEditor.class:153
msgid "Cancel"
msgstr "Annuleren"

#: FBlobEditor.form:39
msgid "Clear"
msgstr "Opschonen"

#: .project:1
msgid "Data bound controls"
msgstr "Data gebonden controls"

#: FBrowser.form:62
msgid "Delete"
msgstr "Verwijderen"

#: FBlobEditor.class:153
msgid "Do you really want to clear blob?"
msgstr "Wil je werkelijk de blob opschonen?"

#: FBrowser.form:86
msgid "End"
msgstr "Einde"

#: DataControl.class:113 DataField.class:19 DataView.class:459
msgid "False"
msgstr "Vals"

#: DataSource.class:424 DataView.class:745
msgid "Invalid value."
msgstr "Ongeldige waarde."

#: FBlobEditor.form:32
msgid "Load"
msgstr "Laad"

#: FBlobEditor.class:138
msgid "Load blob from file"
msgstr "Laad blob van bestand"

#: FBrowser.form:44
msgid "New"
msgstr "Nieuw"

#: FBrowser.form:80
msgid "Next"
msgstr "Volgende"

#: FBrowser.form:74
msgid "Previous"
msgstr "Vorige"

#: FBrowser.form:56
msgid "Refresh"
msgstr "Ververs"

#: FBlobEditor.form:25 FBrowser.form:50
msgid "Save"
msgstr "Opslaan"

#: FBlobEditor.class:123
msgid "Save blob to file"
msgstr "Blob opslaan in bestand"

#: FBrowser.form:68
msgid "Start"
msgstr "Start"

#: DataControl.class:112 DataView.class:459
msgid "True"
msgstr "Waar"

#: FBlobEditor.class:161
msgid "Unable to clear blob"
msgstr "Onmogelijk om blob op te schonen"

#: DataView.class:1089
msgid "Unable to delete record."
msgstr "Onmogelijk om record te verwijderen"

#: FBlobEditor.class:147
msgid "Unable to load file"
msgstr "Onmogelijk om bestand te laden"

#: FBlobEditor.class:132
msgid "Unable to save file"
msgstr "Onmogelijk om bestand op te slaan"

#: DataView.class:909
msgid "Unable to save record."
msgstr "Onmogelijk om record op te slaan"

#: DataView.class:743
msgid "Unable to save value."
msgstr "Onmogelijk om waarde op te slaan"

#: DataControl.class:114
msgid "Unknown"
msgstr "Onbekend"

#: DataSource.class:449
msgid "You must fill all mandatory fields."
msgstr "Je dient alle verplichte velden in te vullen"

